const Course = require('../models/course')

module.exports.addCourse = (body) => {
    console.log(body) // show in the console

    // create a new object called newCourse based on our Course model. Each of its fields values will come from the request body
    let newCourse = new Course({
        name: body.name,
        description: body.description,
        price: body.price,
    })

    // use .save() to save our newCourse object to our database. if saving is NOT successful, and error message will be contained inside of the error parrameter passed to .then()

    // if the error parrameter has a value, then it will render true in our if statement, and we can send the error message as response

    // if saving is successful, the error parameter will be empty, and thus render false. This will cause our else statement to be run instead and we can confirm successful course creation as our response.
    return newCourse.save().then((course, error) => {
        if(error){ // if there's and error
            return false;
        }else{
            return true;
        }
    })    
}

// module.exports.getAllActive = () => {
//     return Course.find({ isActive: true }).then(result => {
//         return result;
//     })
// }
module.exports.getCourses = () => {
    return Course.find({ }).then(result => {
        return result;
    })
}


module.exports.getSpecificCourse = (courseId) => {
    return Course.findById(courseId).then(result => {
        return result;
    })
}

module.exports.updateCourse = (courseId, body) => {
    let updatedCourse = {
        name: body.name,
        description: body.description,
        price: body.price,
    }

    return Course
    .findByIdAndUpdate(courseId, updatedCourse)
    .then((course, error) => error ? false : true )
}

module.exports.archiveCourse = (courseId, body) => {
    let updatedCourse = {
        isActive: body.isActive,
    }

    return Course
    .findByIdAndUpdate(courseId, updatedCourse)
    .then((course, error) => error ? false : true )
}
