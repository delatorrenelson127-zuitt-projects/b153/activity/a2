const express = require('express')
const router = express.Router()
const courseController = require('../controllers/course')
const auth = require('../auth')


// router.get('/', (req, res)=>{
//     courseController.getAllActive().then(resultFromController => res.send(resultFromController))
// })

// Get all courses
router.get('/', (req, res)=>{
    courseController
    .getCourses()
    .then(resultFromController => res.send(resultFromController))
})

// route to get specific course
router.get('/:courseId', (req, res)=>{
    // '/:courseId' here is called a 'WildCard' and its value is anything that is added at the end of localhost:4000/courses
    courseController
    .getSpecificCourse(Object.values(req.params)[0])
    .then(resultFromController => res.send(resultFromController))
})


// route to create a new course
// when a user sends a specific method to a specific endpoint (in this case a POST request to our /courses endpoint) the code within this route will be run
router.post('/', auth.verify, (req,res)=>{
    // auth.verify here is something called 'middleware'
    // Middleware is any function that mst firt be resolved.
    // we also pass the req.body to addCourse as part of the data that i needs
    // Once addCourse has resolved, .then() can send whatever it returns (true or false) as its response        
    if(auth.decode(req.headers.authorization).isAdmin){
        courseController
        .addCourse(req.body)
        .then(resultFromController => res.send(resultFromController))        
    }else{ res.send('Non Admin')}   
})

// route to update a single course
router.put('/:courseId', auth.verify, (req, res) =>{
    if(auth.decode(req.headers.authorization).isAdmin){
        console.log('Admin')
        courseController
        .updateCourse(req.params.courseId, req.body)
        .then(resultFromController => {res.send(resultFromController)})
    }else{ res.send('Non Admin')}   
})


// route to archive course
router.put('/arvhive/:courseId', auth.verify, (req, res) =>{
    if(auth.decode(req.headers.authorization).isAdmin){
        console.log('Admin')
        courseController
        .archiveCourse(req.params.courseId, req.body)
        .then(resultFromController => res.send(resultFromController))
    }else{ res.send('Non Admin')}     
})

// route - carry our request directs to end point
// A route refers to how an application responds to a client request to a particular endpoint, which is a URI and a specific HTTP request method.
// export the router
module.exports = router;
